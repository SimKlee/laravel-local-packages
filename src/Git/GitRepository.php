<?php

declare(strict_types=1);

namespace SimKlee\LaravelLocalPackages\Git;

use Illuminate\Support\Str;

class GitRepository
{
    public readonly string $protocol;
    public readonly string $platform;
    public readonly string $vendor;
    public readonly string $package;

    public function __construct(public readonly string $repository)
    {
        if (Str::startsWith($this->repository, 'git@')) {
            $this->protocol = 'git';
            $package        = Str::substr($this->repository, Str::length('git@'));
            [$this->platform, $path] = explode(':', $package);
            [$vendor, $package] = explode('/', $path);
            $this->vendor  = Str::lower($vendor);
            $this->package = Str::lower(Str::substr($package, 0, -4));
        } elseif (Str::startsWith($this->repository, 'https://')) {
            $this->protocol = 'https';
            $package        = Str::substr($this->repository, Str::length('https://'));
            [$this->platform, $vendor, $package] = explode('/', $package);
            $this->vendor  = Str::lower($vendor);
            $this->package = Str::lower(Str::substr($package, 0, -4));
        }
    }

    public function packageName(): string
    {
        return sprintf('%s/%s', $this->vendor, $this->package);
    }
}