<?php

declare(strict_types=1);

namespace SimKlee\LaravelLocalPackages\Composer;

use Illuminate\Support\Facades\File;

class ComposerJson
{
    private array $data = [];

    public function __construct(public readonly string $file)
    {
        $this->data = json_decode(File::get($this->file), true, JSON_THROW_ON_ERROR);
    }

    public function name(): string|null
    {
        return $this->data['name'] ?? null;
    }

    public function type(): string|null
    {
        return $this->data['type'] ?? null;
    }

    public function description(): string|null
    {
        return $this->data['description'] ?? null;
    }

    public function keywords(): array|null
    {
        return $this->data['keywords'] ?? null;
    }

    public function license(): string|null
    {
        return $this->data['license'] ?? null;
    }

    public function minimumStability(): string|null
    {
        return $this->data['minimum-stability'] ?? null;
    }

    public function preferStable(): bool|null
    {
        return $this->data['prefer-stable'] ?? null;
    }

    public function require(): array|null
    {
        return $this->data['require'] ?? null;
    }

    public function requireDev(): array|null
    {
        return $this->data['require-dev'] ?? null;
    }

    public function repositories(): array|null
    {
        return $this->data['repositories'] ?? null;
    }

    public function hasRepository(string $name): bool
    {
        $repositories = $this->repositories();

        foreach ($repositories ?? null as $repository) {
            if ($repository['name'] === $name) {
                return true;
            }
        }

        return false;
    }

    public function addRepository(string $name, string $type, string $url): bool
    {
        if ($this->hasRepository($name)) {
            return false;
        }

        if (!isset($this->data['repositories'])) {
            $this->data['repositories'] = [];
        }

        $this->data['repositories'][] = [
            'name' => $name,
            'type' => $type,
            'url'  => $url,
        ];

        return true;
    }

    public function autoload(): array|null
    {
        return $this->data['autoload'] ?? null;
    }

    public function autoloadDev(): array|null
    {
        return $this->data['autoload-dev'] ?? null;
    }

    public function scripts(): array|null
    {
        return $this->data['scripts'] ?? null;
    }

    public function extra(): array|null
    {
        return $this->data['extra'] ?? null;
    }

    public function config(): array|null
    {
        return $this->data['config'] ?? null;
    }

    public function save(): bool
    {
        return File::put($this->file, json_encode($this->data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)) !== false;
    }
}