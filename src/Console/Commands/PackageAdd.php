<?php

declare(strict_types=1);

namespace SimKlee\LaravelLocalPackages\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use SimKlee\LaravelLocalPackages\Composer\ComposerJson;
use SimKlee\LaravelLocalPackages\Git\GitRepository;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class PackageAdd extends Command
{
    /**
     * @var string
     */
    protected $signature = 'package:add {repository}';

    /**
     * @var string
     */
    protected $description = 'Adds a package to the packages directory.';

    public function handle(): int
    {
        $repository = new GitRepository($this->argument('repository'));

        if (File::isDirectory(base_path(sprintf('packages/%s/%s', $repository->vendor, $repository->package))) === false) {
            $done = $this->cloneRepository($repository);
        } else {
            $done = true;
            $this->components->info('Package already exists!');
        }

        if ($done) {
            $composer = new ComposerJson(base_path('composer.json'));
            if ($composer->hasRepository($repository->packageName()) === false) {
                $this->addRepository($repository, $composer);
            } else {
                $this->components->info('Repository already exists in composer.json!');
            }

            if (File::isDirectory(base_path(sprintf('vendor/%s/%s', $repository->vendor, $repository->package))) === false) {
                $this->requirePackage($repository);
            } else {
                $this->components->info('Package already exists in vendor!');
            }
        }

        return self::SUCCESS;
    }

    private function cloneRepository(GitRepository $repository): bool
    {
        if ($this->components->confirm('Clone repository', true)) {
            if (File::isDirectory(base_path('packages')) === false) {
                File::makeDirectory(base_path('packages'));
                $this->components->info('Created directory ' . base_path('packages'));
            }

            if (File::exists(base_path('packages/.gitignore')) === false) {
                if ($this->components->confirm('Create .gitignore file', true)) {
                    File::put(base_path('packages/.gitignore'), '*' . PHP_EOL . '!.gitignore');
                    $this->components->info('Created file .gitignore');
                }
            }

            if (File::isDirectory(base_path('packages/' . $repository->vendor)) === false) {
                File::makeDirectory(base_path('packages/' . $repository->vendor));
                $this->components->info('Created directory ' . base_path('packages/' . $repository->vendor));
            }

            $process = new Process(
                command: ['git', 'clone', $repository->repository],
                cwd    : base_path('packages/' . $repository->vendor)
            );
            $process->run(function ($type, $buffer) {
                if ($this->output->getVerbosity() === OutputInterface::VERBOSITY_VERBOSE) {
                    $this->info($buffer);
                }
            });

            if ($process->isSuccessful()) {
                $this->components->info('Cloned repository successful.');
            } else {
                $this->components->error($process->getErrorOutput());
            }

            return $process->isSuccessful();
        }

        return false;
    }

    private function addRepository(GitRepository $repository, ComposerJson $composer): void
    {
        $name = $repository->packageName();
        if ($this->components->confirm('Add repository to composer.json', true)) {
            $added = $composer->addRepository(
                name: $name,
                type: 'path',
                url : sprintf('packages/%s/%s', $repository->vendor, $repository->package)
            );
            if ($added) {
                $composer->save();
                $this->components->info('Added repository to composer.json successful.');
            }
        }
    }

    private function requirePackage(GitRepository $repository): void
    {
        if ($this->components->confirm('Require package via composer', true)) {
            $process = new Process(['composer', 'require', $repository->packageName()], base_path(''));
            $process->run(function ($type, $buffer) {
                if ($this->output->getVerbosity() === OutputInterface::VERBOSITY_VERBOSE) {
                    $this->info($buffer);
                }
            });

            if ($process->isSuccessful()) {
                $this->components->info('Required package successful.');
            } else {
                $this->components->error('Check errors above!');
            }
        }
    }
}