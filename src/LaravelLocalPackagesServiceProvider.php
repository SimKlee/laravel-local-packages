<?php

declare(strict_types=1);

namespace SimKlee\LaravelLocalPackages;

use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelLocalPackages\Console\Commands\PackageAdd;

class LaravelLocalPackagesServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
        $this->commands([
            PackageAdd::class,
        ]);
    }
}
