# Laravel Local Packages

Helper for developing local packages in Laravel.

## Functionality
- Creates a directory `packages` for the local composer packages.
- Creates a `.gitignore` file in packages directory to prevent committing files in laravel project.
- Creates a subdirectoy `vendor/package` in `packages`.
- Clone the git repository into `packages/vendor/package`.
- Add repository information into `composer.json` (section `respoitories`).
- Require repository via composer.

## Usage
```shell
php artisan package:add REPOSITORY
```
